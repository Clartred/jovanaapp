﻿namespace Biblioteka
{
    partial class IzmeniKnjigu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Naziv = new System.Windows.Forms.TextBox();
            this.Autor = new System.Windows.Forms.TextBox();
            this.Izdavac = new System.Windows.Forms.TextBox();
            this.BrojStranica = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.Potvrdi = new System.Windows.Forms.Button();
            this.Odustani = new System.Windows.Forms.Button();
            this.Zanr = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // Naziv
            // 
            this.Naziv.Location = new System.Drawing.Point(59, 25);
            this.Naziv.Name = "Naziv";
            this.Naziv.Size = new System.Drawing.Size(103, 20);
            this.Naziv.TabIndex = 0;
            // 
            // Autor
            // 
            this.Autor.Location = new System.Drawing.Point(59, 86);
            this.Autor.Name = "Autor";
            this.Autor.Size = new System.Drawing.Size(103, 20);
            this.Autor.TabIndex = 1;
            // 
            // Izdavac
            // 
            this.Izdavac.Location = new System.Drawing.Point(59, 157);
            this.Izdavac.Name = "Izdavac";
            this.Izdavac.Size = new System.Drawing.Size(103, 20);
            this.Izdavac.TabIndex = 2;
            // 
            // BrojStranica
            // 
            this.BrojStranica.Location = new System.Drawing.Point(59, 279);
            this.BrojStranica.Name = "BrojStranica";
            this.BrojStranica.Size = new System.Drawing.Size(103, 20);
            this.BrojStranica.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(92, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(34, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Naziv";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(92, 70);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 7;
            this.label2.Text = "Autor";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(81, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 13);
            this.label3.TabIndex = 8;
            this.label3.Text = "Izdavac";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(92, 195);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(29, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Zanr";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(81, 252);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(67, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "Broj Stranica";
            // 
            // Potvrdi
            // 
            this.Potvrdi.Location = new System.Drawing.Point(12, 336);
            this.Potvrdi.Name = "Potvrdi";
            this.Potvrdi.Size = new System.Drawing.Size(75, 23);
            this.Potvrdi.TabIndex = 11;
            this.Potvrdi.Text = "Potvrdi";
            this.Potvrdi.UseVisualStyleBackColor = true;
            this.Potvrdi.Click += new System.EventHandler(this.Potvrdi_Click);
            // 
            // Odustani
            // 
            this.Odustani.Location = new System.Drawing.Point(141, 336);
            this.Odustani.Name = "Odustani";
            this.Odustani.Size = new System.Drawing.Size(75, 23);
            this.Odustani.TabIndex = 12;
            this.Odustani.Text = "Odustani";
            this.Odustani.UseVisualStyleBackColor = true;
            this.Odustani.Click += new System.EventHandler(this.Odustani_Click);
            // 
            // Zanr
            // 
            this.Zanr.FormattingEnabled = true;
            this.Zanr.Items.AddRange(new object[] {
            "Science - fiction",
            "Thriller",
            "Horror",
            "Romance",
            "Western",
            "History",
            "Mystery",
            "Musical",
            "Biography",
            "Adventure",
            "Action",
            "Documentary",
            "Drama",
            "Sport",
            "Family"});
            this.Zanr.Location = new System.Drawing.Point(59, 211);
            this.Zanr.Name = "Zanr";
            this.Zanr.Size = new System.Drawing.Size(103, 21);
            this.Zanr.TabIndex = 13;
            // 
            // IzmeniKnjigu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(250, 384);
            this.Controls.Add(this.Zanr);
            this.Controls.Add(this.Odustani);
            this.Controls.Add(this.Potvrdi);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BrojStranica);
            this.Controls.Add(this.Izdavac);
            this.Controls.Add(this.Autor);
            this.Controls.Add(this.Naziv);
            this.Name = "IzmeniKnjigu";
            this.Text = "Izmeni knjigu";
            this.Load += new System.EventHandler(this.IzmeniKnjige_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox Naziv;
        private System.Windows.Forms.TextBox Autor;
        private System.Windows.Forms.TextBox Izdavac;
        private System.Windows.Forms.TextBox BrojStranica;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button Potvrdi;
        private System.Windows.Forms.Button Odustani;
        private System.Windows.Forms.ComboBox Zanr;
    }
}