﻿using Biblioteka.Model;
using Biblioteka.Repository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka.Servis
{
    class ClanServis
    {

        public void UnesiClana(Clan clan)
        {
            ClanRepository repository = new ClanRepository();
            repository.UnesiClana(clan);
        }

        public void IzbrisiClana(string jmbg)
        {
            ClanRepository repository = new ClanRepository();
            repository.IzbrisiClana(jmbg);
        }

        public void IzmeniClana(Clan clan)
        {
            ClanRepository repository = new ClanRepository();
            repository.IzmeniClana(clan);
        }
        public Clan DajClana(string jmbg)
        {
            ClanRepository repository = new ClanRepository();
            return repository.DajClana(jmbg);
        }

        public DataTable DajSveClanove()
        {
            ClanRepository repository = new ClanRepository();
            return repository.DajSveClanove();
        }

    }
}
