﻿using Biblioteka.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka.Repository
{
    class ClanRepository
    {
        public void UnesiClana(Clan clan)
        {
            DBKonekcija db = new DBKonekcija();
            using (SqlConnection konekcija = db.DajKonekciju())
            {
                using (SqlCommand komanda = new SqlCommand())
                {
                    string InsertIntoQuery = "INSERT INTO CLAN(jmbg, ime, prezime, adresa) VALUES (@jmbg, @ime, @prezime, @adresa)";
                    komanda.Connection = konekcija;  // <== lacking jmbg ime prezime adresa 
                    komanda.CommandType = CommandType.Text;
                    komanda.CommandText = InsertIntoQuery;
                    komanda.Parameters.AddWithValue("@jmbg", clan.Jmbg);
                    komanda.Parameters.AddWithValue("@ime", clan.Ime);
                    komanda.Parameters.AddWithValue("@prezime", clan.Prezime);
                    komanda.Parameters.AddWithValue("@adresa", clan.Adresa);
                    konekcija.Open();
                    komanda.ExecuteNonQuery();
                    konekcija.Close();

                }
            }

        }

        public void IzbrisiClana(string jmbg)
        {
            try
            {
                using (SqlConnection con = new DBKonekcija().DajKonekciju())
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("DELETE FROM CLAN WHERE JMBG = '" + jmbg + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            finally { }

        }

        public void IzmeniClana(Clan clan)
        {

            DBKonekcija db = new DBKonekcija();
            //bolje je new DBKonekcija().DajKonekciju() 
            using (SqlConnection konekcija = db.DajKonekciju())
            {
                using (SqlCommand komanda = new SqlCommand())
                {
                    string updateQuery =
                        "UPDATE CLAN set ime = @ime, prezime = @prezime, adresa = @adresa WHERE JMBG = '" +
                        clan.Jmbg + "'";
                    komanda.Connection = konekcija;  // <== lacking
                    komanda.CommandType = CommandType.Text;
                    komanda.CommandText = updateQuery;
                    komanda.Parameters.AddWithValue("@ime", clan.Ime);
                    komanda.Parameters.AddWithValue("@prezime", clan.Prezime);
                    komanda.Parameters.AddWithValue("@adresa", clan.Adresa);
                    
                    try
                    {
                        konekcija.Open();
                        komanda.ExecuteNonQuery();
                    }
                    catch (SqlException)
                    {

                    }
                    finally
                    {
                        konekcija.Close();
                    }
                }
            }

        }
        public Clan DajClana(string jmbg)
        {
            return null;
        }

        public DataTable DajSveClanove()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new DBKonekcija().DajKonekciju())
            {
                using (SqlCommand cmd = new SqlCommand("Select * from clan", con))
                {
                    con.Open();
                    SqlDataReader sqlDataReader = cmd.ExecuteReader();

                    dt.Load(sqlDataReader);
                }
            }
            return dt;

        }
    }
}
