﻿using Biblioteka.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka.Repository
{
    class KnjigaRepository
    {

        public void UnesiKnjigu(Knjiga knjiga)
        {
            DBKonekcija db = new DBKonekcija();
            //skracena verzija -----> new DBKonekcija().DajKonekciju() 
            using (SqlConnection konekcija = db.DajKonekciju())
            {
                using (SqlCommand komanda = new SqlCommand())
                {
                    //, jbmg_clana
                    string InsertIntoQuery = "INSERT INTO KNJIGA(naziv, autor, izdavac, zanr, broj_stranica) VALUES (@naziv, @autor, @izdavac, @zanr, @broj_stranica)";
                    komanda.Connection = konekcija;  // <== lacking
                    komanda.CommandType = CommandType.Text;
                    komanda.CommandText = InsertIntoQuery;
                    komanda.Parameters.AddWithValue("@autor", knjiga.Autor);
                    komanda.Parameters.AddWithValue("@naziv", knjiga.Naziv);
                    komanda.Parameters.AddWithValue("@izdavac", knjiga.Izdavac);
                    komanda.Parameters.AddWithValue("@zanr", knjiga.Zanr);
                    komanda.Parameters.AddWithValue("@broj_stranica", knjiga.BrojStranica);

                    try
                    {
                        konekcija.Open();
                        komanda.ExecuteNonQuery();
                    }
                    catch (SqlException)
                    {

                    }
                    finally
                    {
                        konekcija.Close();
                    }
                }
            }
        }

        public void IzbrisiKnjigu(int id)
        {
            try
            {
                using (SqlConnection con = new DBKonekcija().DajKonekciju())
                {
                    con.Open();
                    using (SqlCommand command = new SqlCommand("DELETE FROM KNJIGA WHERE ID = '" + id + "'", con))
                    {
                        command.ExecuteNonQuery();
                    }
                    con.Close();
                }
            }
            finally { }
            
        }
    
        

        public void IzmeniKnjigu(Knjiga knjiga)
        {
            DBKonekcija db = new DBKonekcija();
            //bolje je new DBKonekcija().DajKonekciju() 
            using (SqlConnection konekcija = db.DajKonekciju())
            {
                using (SqlCommand komanda = new SqlCommand())
                {
                    //, jbmg_clana
                    string updateQuery =
                        "UPDATE KNJIGA set naziv = @naziv, autor = @autor, izdavac = @izdavac, zanr = @zanr, broj_stranica = @broj_stranica WHERE ID = '" +
                        knjiga.Id + "'";
                    komanda.Connection = konekcija;  // <== lacking
                    komanda.CommandType = CommandType.Text;
                    komanda.CommandText = updateQuery;
                    komanda.Parameters.AddWithValue("@autor", knjiga.Autor);
                    komanda.Parameters.AddWithValue("@naziv", knjiga.Naziv);
                    komanda.Parameters.AddWithValue("@izdavac", knjiga.Izdavac);
                    komanda.Parameters.AddWithValue("@zanr", knjiga.Zanr);
                    komanda.Parameters.AddWithValue("@broj_stranica", knjiga.BrojStranica);

                    try
                    {
                        konekcija.Open();
                        komanda.ExecuteNonQuery();
                    }
                    catch (SqlException)
                    {

                    }
                    finally
                    {
                        konekcija.Close();
                    }
                }
            }
        }
        public Knjiga DajKnjigu(int id)
        {
            return null;
        }

        public DataTable DajSveKnjige()
        {

            DataTable dt = new DataTable();
            using (SqlConnection con = new DBKonekcija().DajKonekciju())
            {
                string query = 
                    "select id as ID, naziv as Naziv, autor as Autor, izdavac as Izdavac, zanr as Zanr, broj_stranica as Broj_Stranica, jmbg_clana as Jbmg_Clana," +
                    " CASE WHEN izdata = 1 THEN 'Izdata' ELSE 'Nije izdata' END as Izdata from knjiga";
                //CASE WHEN IsMale = 1 THEN 'Yes' ELSE 'No' END AS IsMale
                using (SqlCommand cmd = new SqlCommand(query, con))                                                                                   
                {
                    con.Open();
                    SqlDataReader sqlDataReader = cmd.ExecuteReader();

                    dt.Load(sqlDataReader);
                }
            }
            return dt;
        }

        public void IzdavanjeKnjige(int id)
        { 
            DBKonekcija db = new DBKonekcija();
            //bolje je new DBKonekcija().DajKonekciju() 
            using (SqlConnection konekcija = db.DajKonekciju())
            {
                using (SqlCommand komanda = new SqlCommand())
                {
                 string updateQuery =
                        "UPDATE KNJIGA set izdata = @izdata WHERE ID = '" + id + "'";
                    komanda.Connection = konekcija;  
                    komanda.CommandType = CommandType.Text;
                    komanda.CommandText = updateQuery;
                    komanda.Parameters.AddWithValue("@izdata", 1);
                    
                    try
                    {
                        konekcija.Open();
                        komanda.ExecuteNonQuery();
                    }
                    catch (SqlException)
                    {

                    }
                    finally
                    {
                        konekcija.Close();
                    }
                }
            }

        }
        public void VracanjeKnjige(int id)
        {
            DBKonekcija db = new DBKonekcija();
            //bolje je new DBKonekcija().DajKonekciju() 
            using (SqlConnection konekcija = db.DajKonekciju())
            {
                using (SqlCommand komanda = new SqlCommand())
                {
                    string updateQuery =
                        "UPDATE KNJIGA set izdata = @izdata WHERE ID = '" + id + "'";
                    komanda.Connection = konekcija;
                    komanda.CommandType = CommandType.Text;
                    komanda.CommandText = updateQuery;
                    komanda.Parameters.AddWithValue("@izdata", 0);

                    try
                    {
                        konekcija.Open();
                        komanda.ExecuteNonQuery();
                    }
                    catch (SqlException)
                    {

                    }
                    finally
                    {
                        konekcija.Close();
                    }
                }
            }

        }

    }
}
