﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka.Model
{
    public class Clan
    {

        public Clan(string v) { }

        public Clan(string jmbg, string ime, string prezime, string adresa)
        {
            this.Jmbg = jmbg;
            this.Ime = ime;
            this.Prezime = prezime;
            this.Adresa = adresa;
        }

        public string Jmbg { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string Adresa{ get; set; }
    }
}
